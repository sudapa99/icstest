import React from "react";
import './App.css';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import PalceList from "./palceList";
import WatchPalce from "./watchPalce";


function App() {

  return (
    <Router>
      <Switch>
        <Route exact path={["/", "/home"]} component={PalceList} />
        <Route exact path="/:id" component={WatchPalce} />
      </Switch>
    </Router>
  );
}

export default App;
