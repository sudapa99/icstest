import React from "react";
import './App.css';
import datalist from './example_data.json'
import { Avatar, Badge, Button, Col, Descriptions, Input, Layout, List, Menu, Rate, Row, Select, Space } from 'antd';
import { UnorderedListOutlined, UserOutlined, LogoutOutlined, BellOutlined, LeftOutlined, CalendarOutlined } from '@ant-design/icons';
import { NavLink } from "react-router-dom";

function WatchPalce(props) {

    console.log(props)

    const { Header, Content, Sider } = Layout;
    const { SubMenu } = Menu

    const palceId = Number(props.match.params.id)

    const selectPalce = datalist.map(data => {
        return data
    }).filter(item => {
        return item.id === palceId
    })

    console.log(selectPalce)

    return (
        <Layout style={{ backgroundColor: '#fff' }}>
            <Sider className="sider" style={{ minHeight: '100vh', backgroundColor: '#fff', textAlign: 'center' }}>
                <div>
                    <img className="logo" src='/logoics.png' />
                </div>
                <div>
                    <NavLink to='/'>
                        <Button type='primary' className='btnpalce' size='large' icon={<UnorderedListOutlined />} style={{ backgroundColor: '#0f1E56', borderRadius: 10 }}></Button>
                        <br />
                        <label className='palce'>Palce</label>
                    </NavLink>
                </div>
            </Sider>
            <Layout style={{ backgroundColor: '#fff' }}>
                <Header className="site-layout-sub-header-background" style={{ height: 100, backgroundColor: '#134B8A' }} >
                    <Space style={{ float: 'right' }}>
                        <Badge dot style={{ marginTop: 30 }}>
                            <BellOutlined style={{ color: '#fff', fontSize: 25, marginTop: 30 }} />
                        </Badge>
                        <Menu mode="inline" style={{ backgroundColor: '#134B8A', width: 256, padding: 0, marginTop: 5 }}>
                            <SubMenu key="username" icon={<Avatar icon={<UserOutlined />} />} title="Username">
                                <Menu.Item key="logout" icon={<LogoutOutlined />} onClick={console.log('Logout')}>logout</Menu.Item>
                            </SubMenu>
                        </Menu>
                    </Space>
                </Header>
                <Content style={{ margin: '24px 16px 0' }}>
                    <NavLink to='/' style={{ color: '#fff' }}>
                        <Button type="primary" shape="round" icon={<LeftOutlined />} size='large' style={{ backgroundColor: '#134B8A' }}>
                            BACK
                        </Button>
                    </NavLink>
                </Content>
                <br />
                <Content style={{ padding: 50, backgroundColor: "#C4D3E9", borderRadius: 25 }}>
                    <Row gutter={[16, 8]}>
                        {selectPalce.map(data => {
                            return <>
                                <Col flex="1 1 200px" className="style-con" style={{ padding: 0 }}>
                                    <img className='profile' src={data.profile_image_url} />
                                    <br />
                                    <div className="style-des">
                                        <label className='name-palce-watch'>{data.name}</label>
                                        <label className='rate-watch'>{data.rating}</label>
                                        <br />
                                        <label className='title-address'>address:</label>
                                        <label className='address'>{data.address}</label>
                                        <br />
                                        <br />
                                        <label className='title-time'>Opening Hour:</label>

                                        {data.operation_time.map(time => {
                                            return <List style={{ marginLeft: 140 }}>{time.day}: {time.time_open} AM - {time.time_close} PM</List>
                                        })}


                                    </div>
                                </Col>
                                <Col flex="0 1 500px" className="style-img">
                                    <label className="title-img">Images</label>
                                    <br />
                                    <br />
                                    <div className="style-images-palce" >
                                        {data.images.map(img => {
                                        return <>
                                            <img className='images-palce' src={img} />
                                            <br />
                                        </>
                                    })}
                                    </div>
                                    
                                </Col>
                            </>
                        })}
                    </Row>
                </Content>
            </Layout>
        </Layout>
    )
}

export default WatchPalce;