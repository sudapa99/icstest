import React, { useState } from 'react';
import 'antd/dist/antd.css';
import './App.css';
import { Avatar, Badge, Button, Col, Descriptions, Input, Layout, Menu, Pagination, Rate, Row, Select, Space } from 'antd';
import { UnorderedListOutlined, UserOutlined, LogoutOutlined, BellOutlined, SearchOutlined, CalendarOutlined } from '@ant-design/icons';
import { Option } from 'antd/lib/mentions';
import datalits from './example_data.json'
import { NavLink } from 'react-router-dom';

function PalceList() {

    const { Header, Content, Sider } = Layout;
    const { SubMenu } = Menu
    const [searchText, setSearchText] = useState('');
    const [type, setType] = useState(null)

    const search = (value) => {
        setType(value)
    }

    const searchName = datalits.filter((data) => {
        if (type !== null) {
            return data.categories.includes(type)
        } else {
            return data.name.toLowerCase().includes(searchText.toLowerCase())
        }
    });

    const dataList = searchName.map(data => {
        return <Col span={8}>
            <NavLink to={`/${data.id}`} style={{ color: '#000' }}>
                <div className='style-col'>
                    <img className='pro-img' src={data.profile_image_url} />
                    <label className='name-palce'>{data.name}</label>
                    <br />
                    <Space className='time'>
                        <CalendarOutlined />
                        {data.operation_time.map(time => {
                            return time.time_open
                        }).find(topen => {
                            return <label className='time'>{topen}</label>
                        })} AM
                        - {data.operation_time.map(time => {
                            return time.time_close
                        }).find(tclose => {
                            return <label className='time'>{tclose}</label>
                        })} PM

                    </Space>
                    <div className='style-rate'>
                        <label className='rate'>{data.rating}</label>
                    </div>
                    <br />
                    <br />
                    <div className='style-grid'>
                        <Row style={{ textAlign: 'center' }}>
                            {data.images.map(img => {
                                return <Col span={8}>
                                    <img className='images' src={img} />
                                </Col>
                            })}
                        </Row>
                    </div>
                </div>
            </NavLink>
        </Col>
    })

    const logout = () => {
        console.log('Logout')
    }


    return (
        <Layout style={{ backgroundColor: '#fff' }}>
            <Sider className="sider" style={{ minHeight: '100vh', backgroundColor: '#fff', textAlign: 'center' }}>
                <div>
                    <img className="logo" src='/logoics.png' />
                </div>
                <NavLink to='/'>
                    <Button type='primary' className='btnpalce' size='large' icon={<UnorderedListOutlined />} style={{ backgroundColor: '#0f1E56', borderRadius: 10 }}></Button>
                    <br />
                    <label className='palce'>Palce</label>
                </NavLink>
            </Sider>
            <Layout style={{ backgroundColor: '#fff' }}>
                <Header className="site-layout-sub-header-background" style={{ height: 100, backgroundColor: '#134B8A' }} >
                    <Space style={{ float: 'right' }}>
                        <Badge dot style={{ marginTop: 30 }}>
                            <BellOutlined style={{ color: '#fff', fontSize: 25, marginTop: 30 }} />
                        </Badge>
                        <Menu mode="inline" style={{ backgroundColor: '#134B8A', width: 256, padding: 0, marginTop: 5 }}>
                            <SubMenu key="username" icon={<Avatar icon={<UserOutlined />} />} title="Username" style={{ color: '#fff' }}>
                                <Menu.Item key="logout" icon={<LogoutOutlined />} onClick={logout}>logout</Menu.Item>
                            </SubMenu>
                        </Menu>
                    </Space>
                </Header>
                <Content style={{ margin: '24px 16px 0' }}>
                    <label className='palce-list'>Palce List</label>
                    <Space style={{ float: "right" }}>
                        <Select className='select' size='large' defaultValue="Type" onChange={search}>
                            <Option value="restaurant" >Restaurant</Option>
                            <Option value="bakery" >Bakery</Option>
                            <Option value="cafe" >Cafe</Option>
                        </Select>
                        <label className='or'>|</label>
                        <Input className='style-search' shape="round"
                            placeholder='Search Name...'
                            prefix={<SearchOutlined />}
                            value={searchText}
                            onChange={(e) => { setSearchText(e.target.value) }}
                            style={{ borderRadius: 25 }} />
                    </Space>
                </Content>
                <Content style={{ padding: 50 }}>
                    <Row gutter={[32, 48]}>
                        {dataList}
                    </Row>
                    <br />
                    <Pagination data={dataList} dataLimit={9} style={{ textAlign: 'center' }} />
                </Content>
            </Layout>
        </Layout>
    );
}

export default PalceList;
